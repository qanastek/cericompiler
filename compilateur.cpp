//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INTEGER, BOOLEAN, STRING, FLOAT, DOUBLE};

TOKEN current;				// Current token

FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
unordered_map<string, enum TYPES> DeclaredVariables;	// Store declared variables and their types
unsigned long long TagNumber = 0;

// Avance
void stepForward(void)
{
	current = (TOKEN) lexer->yylex();
}

bool IsDeclared(const char *id)
{
	return DeclaredVariables.find(id) != DeclaredVariables.end();
}


void Error(string s)
{
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

string toStringType(enum type)
{
	switch(type)
	{
		case BOOLEAN:
			return "BOOLEAN";

		case INTEGER:
			return "INTEGER";

		case STRING:
			return "STRING";

		case FLOAT:
			return "FLOAT";

		case DOUBLE:
			return "DOUBLE";
	}
}

string getDefaultValue(enum type)
{
	switch(type)
	{
		case BOOLEAN:
			return "0";

		case INTEGER:
			return "0";

		case STRING:
			return "vide";

		case FLOAT:
			return "0.0";

		case DOUBLE:
			return "0.0";
	}
}

// Program := [VarDeclarationPart] StatementPart
// VarDeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
// To avoid cross references problems :
enum TYPES Expression(void);			// Called by Term() and calls Term()
void Statement(void);
void StatementPart(void);
		
enum TYPES Identifier(void)
{
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();

	return INTEGER;
}

enum TYPES Number(void)
{
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();

	return INTEGER;
}

enum TYPES Factor(void)
{
	enum TYPES type;

	if(current==RPARENT)
	{
		current=(TOKEN) lexer->yylex();
		type=Expression();

		if(current!=LPARENT)
		{
			Error("')' expected");
		}
		else
		{
			current=(TOKEN) lexer->yylex();
		}
	}
	else 
	{
		if (current==NUMBER)
		{
			type=Number();

	     	else
	     	{
				if(current==ID)
				{
					type=Identifier();
				}
				else
				{
					Error("'(' or Number or Letter expected");
				}
			}
		}
	}

	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void)
{
	OPMUL opmul;

	if(strcmp(lexer->YYText(),"*")==0)
	{
		opmul=MUL;
	}
	else if(strcmp(lexer->YYText(),"/")==0)
	{
		opmul=DIV;
	}
	else if(strcmp(lexer->YYText(),"%")==0)
	{
		opmul=MOD;
	}
	else if(strcmp(lexer->YYText(),"&&")==0)
	{
		opmul=AND;
	}
	else
	{
		opmul=WTFM;
	}

	current=(TOKEN) lexer->yylex();

	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(void)
{
	TYPES typeVar1, typeVar2;
	OPMUL mulop;
	typeVar1=Factor();

	while(current==MULOP)
	{
		mulop=MultiplicativeOperator();		// Save operator in local variable
		typeVar2=Factor();

		if(typeVar2!=typeVar1) { Error("Types are differents"); }

		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand

		switch(mulop)
		{
			case AND:

				if(typeVar2!=BOOLEAN)
				{
					Error("BOOLEAN expression expected");
				}

				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;

			case MUL:

				if(typeVar2 != INTEGER) { Error("INTEGER expression expected"); }

				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;

			case DIV:

				if(typeVar2 != INTEGER) { Error("INTEGER expression expected"); }

				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;

			case MOD:

				if(typeVar2 != INTEGER) { Error("INTEGER expression expected"); }

				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;

			default:

				Error("opérateur multiplicatif attendu");
		}
	}

	return typeVar1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void)
{
	OPADD opadd;

	if(strcmp(lexer->YYText(),"+")==0)
	{
		opadd=ADD;
	}
	else if(strcmp(lexer->YYText(),"-")==0)
	{
		opadd=SUB;
	}
	else if(strcmp(lexer->YYText(),"||")==0)
	{
		opadd=OR;
	}
	else
	{
		opadd=WTFA;
	}

	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPES SimpleExpression(void)
{
	enum TYPES typeVar1, typeVar2;
	OPADD adop;
	typeVar1=Term();

	while(current==ADDOP)
	{
		adop=AdditiveOperator();		// Save operator in local variable
		typeVar2=Term();

		if(typeVar2!=typeVar1)
		{
			Error("Types are differents");
		}

		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand

		switch(adop)
		{
			case OR:

				if(typeVar2!=BOOLEAN)
				{
					Error("BOOLEAN expression expected");
				}

				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;

			case ADD:

				if(typeVar2 != INTEGER) { Error("INTEGER expression expected"); }

				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;	

			case SUB:

				if(typeVar2 != INTEGER) { Error("INTEGER expression expected"); }

				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;

			default:

				Error("opérateur additif inconnu");
		}

		cout << "\tpush %rax"<<endl;			// store result
	}

	return typeVar1;
}

// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void)
{
	vector<string> variablesCurrentType;

	stepForward();

	if(current != ID) { Error("Identifier expected"); }

	variablesCurrentType.push_back(lexer->YYText());

	stepForward();

	while(current == COMMA)
	{

		if(current != ID) { Error("Identifier expected"); }

		variablesCurrentType.push_back(lexer->YYText());

		stepForward();

	}

	if(current != COLON) { Error("COLON expected"); }

	stepForward();

	if(current != TYPES) { Error("TYPES expected"); }

	string theType = lexer->YYText();

	std::for_each(variablesCurrentType.begin(), variablesCurrentType.end(), [](string * variable)
        {
        	cout << variable << ":\t." << toStringType(theType) << " " << getDefaultValue(theType) << endl;
        }
	);

	stepForward();

}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void)
{
	stepForward();

	if(strcmp(lexer->YYText(),"VAR") == 0) { Error("VAR expected"); }

	VarDeclaration();

	while(current == SEMICOLON)
	{
		VarDeclaration();
	}

	if(current != DOT) { Error("Dot expected"); }

	stepForward();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void)
{
	OPREL oprel;

	if(strcmp(lexer->YYText(),"==") == 0)
	{
		oprel = EQU;
	}
	else if(strcmp(lexer->YYText(),"!=") == 0)
	{
		oprel = DIFF;
	}
	else if(strcmp(lexer->YYText(),"<") == 0)
	{
		oprel = INF;
	}
	else if(strcmp(lexer->YYText(),">") == 0)
	{
		oprel = SUP;
	}
	else if(strcmp(lexer->YYText(),"<=") == 0)
	{
		oprel = INFE;
	}
	else if(strcmp(lexer->YYText(),">=") == 0)
	{
		oprel = SUPE;
	}
	else
	{
		oprel = WTFR;
	}

	stepForward();

	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(void)
{
	enum TYPES typeVar1, typeVar2;
	unsigned long long tag;
	OPREL oprel;

	typeVar1 = SimpleExpression();

	if(current == RELOP)
	{
		tag = ++TagNumber;
		oprel = RelationalOperator();
		typeVar2 = SimpleExpression();

		if(typeVar2 != typeVar1) { Error("cannot compare two differents types"); }

		cout << "\tpop %rax" << endl;
		cout << "\tpop %rbx" << endl;
		cout << "\tcmpq %rax, %rbx" << endl;	// Compare les deux variables

		switch(oprel)
		{
			// ==
			case EQU:
				cout << "\tje Vrai" << tag << endl;
				break;

			// !=
			case DIFF:
				cout << "\tjne Vrai" << tag << endl;
				break;

			// >
			case SUPE:
				cout << "\tjae Vrai" << tag << endl;
				break;

			// <=
			case INFE:
				cout << "\tjbe Vrai" << tag << endl;
				break;

			// <
			case INF:
				cout << "\tjb Vrai" << tag << endl;
				break;

			// >
			case SUP:
				cout << "\tja Vrai" << tag << endl;
				break;

			// Not allowed operator
			default:
				Error("Unknown operator");
		}

		cout << "\tpush $0" << endl;			// push faux
		cout << "\tjmp Suite" << tag << endl;	// saute vrai

		cout << "Vrai" << tag << ":" << endl;
		cout << "\tpush $0xFFFFFFFFFFFFFFFF" << endl;	// push faux

		cout << "Suite" << tag << ":" << endl;	// fin de la comparaison

		return BOOLEAN;
	}

	return typeVar1;
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void)
{
	enum TYPES typeVar1, typeVar2;
	string variable;

	if(current != ID) { Error("Identifier expected"); }

	if(!IsDeclared(lexer->YYText()))
	{
		cerr << "Variable '" << lexer->YYText() << "' non declared" << endl;
		exit(-1);
	}

	variable = lexer->YYText();
	typeVar1 = DeclaredVariables[variable];
	stepForward();

	if(current != ASSIGN)
	{
		Error("':=' expected");
	}

	stepForward();
	typeVar2 = Expression();

	if(typeVar2 != typeVar1)
	{
		Error("The types are differents : " + toStringType(typeVar1) + " different of " + toStringType(typeVar2));
	}

	cout << "\tpop "<<variable<<endl;
}

// DisplayStatement := "DISPLAY" Expression
void DisplayStatement(void)
{
	stepForward();

	cout << "\tpop %rdx" << endl;

	if(Expression() == INTEGER)
	{
		cout << "\tmovq $FormatUnsignedInt, %rsi\t# \"%llu\\n\"" << endl;
	}
	else if(Expression() == STRING)
	{
		Error("Not coded.");
	}
	else if(Expression() == FLOAT or Expression() == DOUBLE)
	{
		cout << "\tmovq $FormatFloat64, %rsi\t# \"%llu\\n\"" << endl;
	}
	else if(Expression() == CHAR)
	{
		cout << "\tmovq $FormatChar, %rsi\t# \"%llu\\n\"" << endl;
	}
	else if(Expression() == BOOLEAN)
	{
		if (strcmp(lexer->YYText(),"0") == 0)
		{
			cout << "\tmovq $FormatFalse, %rsi\t# \"%llu\\n\"" << endl;
		}
		else if (strcmp(lexer->YYText(),"1") == 0)
		{
			cout << "\tmovq $FormatTrue, %rsi\t# \"%llu\\n\"" << endl;
		}
		else
		{
			Error("BOOLEAN expected !");
		}
	}
	else
	{
		Error("Display type not allowed !");
	}

	cout << "\tmovl	$1, %edi" << endl;
	cout << "\tmovl	$0, %eax" << endl;
	cout << "\tcall	__printf_chk@PLT" << endl;
}

// ForStatement := "For" ID ":=" Expression ("TO"|"DOWNTO") Expression "DO" Statement
void ForStatement(void)
{
	unsigned long long tag = TagNumber++;

	if (strcmp(lexer->YYText(), "FOR") != 0) Error("FOR expected");

	cout << "ForInit" << tag << ":" << endl;

	stepForward();

	string variable = lexer->YYText(); // Récupère le nom de la variable

	AssignementStatement();

	if (strcmp(lexer->YYText(), "TO") != 0) Error("TO expected");

	stepForward();

	Expression();

	cout << "\tpop %rax" << endl; // Récupère le résultat goal

	cout << "For" << tag << " :" << endl;

	cout << "\tcmpq %rax, " << variable << endl; // Regarde si i respecte le goal
	cout << "\tjge SuiteFor" << tag << endl; // Si il à dépassé ou il est au goal   jump vers SuiteFor

	if (strcmp(lexer->YYText(), "DO") != 0) Error("DO expected");

	stepForward();

	Statement();

	cout << "\taddq $1," << variable << endl;
	cout << "\tjmp For" << tag << endl;

	cout << "SuiteFor" << tag << " : " << endl;
}

// WhileStatement := "WHILE" Expression "DO" Statement
void WhileStatement(void)
{
	unsigned long long tag=TagNumber++;

	cout << "While" << tag << ":" << endl;

	stepForward();

	if(Expression() != BOOLEAN)
	{
		Error("BOOLEAN expression expected");
	}

	cout << "\tpop %rax" << endl;
	cout << "\tcmpq $0, %rax" << endl;
	cout << "\tje FinWhile" << tag << endl;

	if(current != KEYWORD || strcmp(lexer->YYText(), "DO") != 0)
	{
		Error("DO expected");
	}

	stepForward();

	Statement();

	cout << "\tjmp While" << tag << endl;
	cout << "FinWhile" << tag << ":" << endl;
}

// BlockStatement := "BEGIN" Statement {";" Statement} "END"
void BlockStatement(void)
{
	stepForward();

	Statement();

	while(current == SEMICOLON)
	{
		stepForward();
		Statement();
	}

	if(current != KEYWORD || strcmp(lexer->YYText(), "END") != 0)
	{
		Error("END expected");
	}

	stepForward();
}

// IfStatement := "IF" Expression "THEN" Statement ["ELSE" Statement]
void IfStatement(void)
{
	unsigned long long tag = TagNumber++;

	current=(TOKEN) lexer->yylex();

	if(Expression() != BOOLEAN)
	{
		Error("BOOLEAN expected");
	}

	cout << "\tpop %rax" << endl;
	cout << "\tcmpq $0, %rax" << endl;
	cout << "\tje Else" << tag << endl;

	if(current != KEYWORD || strcmp(lexer->YYText(),"THEN") != 0)
	{
		Error("'THEN' expected");
	}

	current=(TOKEN) lexer->yylex();

	Statement();

	cout << "\tjmp SuiteIf" << tag << endl;
	cout << "Else" << tag << ":" << endl;

	if(current == KEYWORD && strcmp(lexer->YYText(),"ELSE") == 0)
	{
		stepForward();
		Statement();
	}

	cout << "SuiteIf" << tag << ":" << endl;
}

// Statement := AssignementStatement|DisplayStatement
void Statement(void)
{
	if(current==KEYWORD)
	{
		if(strcmp(lexer->YYText(),"DISPLAY") == 0)
		{
			DisplayStatement();
		}
		else if(strcmp(lexer->YYText(),"IF") == 0)
		{
			IfStatement();
		}
		else if(strcmp(lexer->YYText(),"FOR") == 0)
		{
			ForStatement();
		}
		else if(strcmp(lexer->YYText(),"WHILE") == 0)
		{
			WhileStatement();
		}
		else if(strcmp(lexer->YYText(),"BEGIN") == 0)
		{
			BlockStatement();
		}
		else
		{
			Error("mot clé inconnu");
		}
	}
	else
	{
		if(current == ID)
		{
			AssignementStatement();
		}
		else
		{
			Error("instruction attendue");
		}
	}
	 
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void)
{
	cout << "\t.align 8"<<endl;	// Alignement on addresses that are a multiple of 8 (64 bits = 8 bytes)
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;

	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	Statement();

	while(current==SEMICOLON)
	{
		current=(TOKEN) lexer->yylex();
		Statement();
	}

	if(current != DOT)
	{
		Error("caractère '.' attendu");
	}

	current=(TOKEN) lexer->yylex();
}

// Program := [VarDeclarationPart] StatementPart
void Program(void)
{
	if(current==RBRACKET)
	{
		VarDeclarationPart();
	}

	StatementPart();	
}

int main(void)
{
	cout << "# This code was produced by the CERI Compiler"<<endl;
	cout << ".data"<<endl;

	cout << "FormatUnsignedInt:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers" << endl;
	cout << "FormatFloat64:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers" << endl;
	cout << "FormatChar:\t.string \"%c\"\t# used by printf to display a 8-bit single character" << endl;
	cout << "FormatTrue:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE" << endl;
	cout << "FormatFalse:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE" << endl;

	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();

	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;

	if(current!=FEOF)
	{
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}

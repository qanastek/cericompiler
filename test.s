# This code was produced by the CERI Compiler
.data
FormatString1:	.string "%llu\n"	# used by printf to display 64-bit unsigned integers
a:	.quad 0
b:	.quad 0
	.align 8
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
ForInit0:
	push $0
	pop a
	push $10
	pop %rax
For0 :
	cmpq %rax, a
	jge SuiteFor0
	push $1
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop b
	addq $1,a
	jmp For0
SuiteFor0 : 
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function

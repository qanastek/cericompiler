# CERIcompiler :computer:

*Author: Labrak Yanis*

*Degree: Licence 2 Informatique - Groupe 4*

*School: Université d'Avignon*

**Je souhaite a signalé que mon profile UAPV à était supprimer par erreur et que tout le travail que j'ai effectuer entre la suppression de mon compte et le rollback de la fin de semaine dernière a était effacer. Merci de votre comprehension**

## :warning: Ce compilateur n'est pas fonctionel en dépit de sa bonne volonté. :warning:

**What is done ?**

- [x] comparaison
- [x] WHILE / IF / FOR
- [x] Gestion des types
- [x] Display
- [x] Déclarations de variables typées
- [ ] Caractères + Nombres Flottants
- [ ] Instruction Case